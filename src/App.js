import { DashGrid } from './components/DashGrid';

export const App = () => {
  return (
    <>
      <h1>Lista de Tableros / Dashboards</h1>
      <hr />

      <DashGrid />
    </>
  );
}
