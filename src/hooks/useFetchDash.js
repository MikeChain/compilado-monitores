import { useState, useEffect } from "react"
import { getDash } from "../helpers/getDash"

export const useFetchDash = () => {
  const [state, setState] = useState({
    data: [],
    loading: true
  })

  useEffect(() => {
    getDash().then(arr => setState({
      data: arr,
      loading: false
    }))
  }, [])

  return state
}