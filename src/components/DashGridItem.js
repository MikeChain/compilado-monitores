import React from "react"

export const DashGridItem = ({Tablero, Detalles, URL, Logo}) => {
  const img = !!Logo && Logo[0].url
  return (
    <div className="card animate__animated animate__jackInTheBox">
      <a className="card__a" href={URL} target="_blank" rel="noreferrer">
        { !!Logo && <img className="card__img" src={img} alt={Tablero} /> }
        <p className="card__title">{Tablero}</p>
        { !!Detalles && <p className="card__details">{Detalles}</p> }
      </a>
    </div>
  )
}