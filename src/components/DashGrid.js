import React from "react"
import { useFetchDash } from "../hooks/useFetchDash"
import { DashGridItem } from "./DashGridItem"

export const DashGrid = () => {

  const {data, loading} = useFetchDash()

  return <>
    {loading && <p>Cargando...</p>}

    <div className='card-grid'>
      {
        data.map(({id, fields}) => <DashGridItem
          key={id}
          {...fields}
          />
        )
      }
    </div>
  </>
}