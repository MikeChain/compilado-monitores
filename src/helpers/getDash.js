import Airtable from "airtable"

export const getDash = async () => {
  const base = new Airtable({ apiKey: 'keyG7MxOaJ3i5mqgg' }).base('appwTW4MWCtjw0Fab');
  const table = base('Tableros');

  const records = await table.select({view: "Grid view"}).firstPage();

  return records.map( record => {
    return {id: record.id, fields: record.fields}
  } );
}